{
  # name = "scoreboard-seedbox-cri";

  description = "A scoreboard for those who are sharing the CRI images.";

  inputs = {
    nixpkgs.url = "nixpkgs";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem;

      poetryArgs = poetry2nix: {
        projectDir = ./.;
        overrides = poetry2nix.overrides.withDefaults (self: super: {
          python-jose = super.python-jose.overridePythonAttrs (o: {
            postPatch = ''
              substituteInPlace setup.py --replace "'pytest-runner'," ""
              substituteInPlace setup.py --replace "'pytest-runner'" ""
            '';
          });
        });
      };

      outputs = {
        overlay = final: prev: {
          scoreboard-seedbox-cri =
            final.poetry2nix.mkPoetryApplication (poetryArgs final.poetry2nix);
        };
      };

      multiSystemOuputs = eachDefaultSystem (system:
      {
        defaultPackage = (import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        }).scoreboard-seedbox-cri;

        devShell =
          let
            pkgs = import nixpkgs { inherit system; };
          in
          pkgs.mkShell {
            buildInputs = with pkgs; [
              #(poetry2nix.mkPoetryEnv (poetryArgs poetry2nix))
              poetry
              docker-compose
            ];
          };
      });
    in recursiveUpdate outputs multiSystemOuputs;
}
