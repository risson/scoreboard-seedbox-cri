from django.urls import path

from .views import ScoreboardView, UploadView

urlpatterns = [
    path("scoreboard/", ScoreboardView.as_view()),
    path("upload/", UploadView.as_view()),
]
