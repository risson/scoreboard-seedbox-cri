from django.contrib import admin

from .models import SeedReport


@admin.register(SeedReport)
class SeedReportAdmin(admin.ModelAdmin):
    list_display = ["user", "time", "size"]
    list_filter = ["user"]
