from django.views.generic import TemplateView
from django.shortcuts import render


class IndexView(TemplateView):
    template_name = "landing_page/index.html"

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, self.template_name, context)
