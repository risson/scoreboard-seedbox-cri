# Scoreboard - Seedbox CRI

This project is not affiliated with EPITA nor the CRI.

### Setup a development environment

Install `docker` and `docker-compose` using your distribution's documentation.
Also install `python3.7` and `poetry`.

For those using NixOS, everything is ready in the `shell.nix` file.

#### Initial setup

```sh
$ poetry install
$ cd docker/
$ ./gen_secrets.sh
$ docker-compose up --build
```

#### Regular usage

```sh
$ cd docker/
$ docker-compose up --build
```

#### Services

##### Adminer

```
URL: http://localhost:8010/
System: 'PostgreSQL'
User: 'scoreboard_dev'
Password in: docker/secrets/postgres-passwd
Database: 'scoreboard_dev'
```

##### Website

```
URL: http://localhost:8000/
To login with a user in your fixtures: http://localhost:8000/admin
To create a superuser:
$ cd docker && ./manage.sh createsuperuser
```

#### Formatting

We use `python/black` as a code formatter.

### Acknowledgements

Thanks to Marin 'mareo' Hannache for the `conf/env.py` file!

Thanks Test. 2020 for letting me get inspiration from their source code to build
this!
